﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace TwoFA.Controllers
{
    public class Helpers
    {
        //Generates a random string of length 10
        public static string RandomString()
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            System.Text.StringBuilder res = new StringBuilder();
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                byte[] uintBuffer = new byte[sizeof(uint)];

                for (int i = 0; i < 15; i++)
                {
                    rng.GetBytes(uintBuffer);
                    uint num = BitConverter.ToUInt32(uintBuffer, 0);
                    res.Append(valid[(int)(num % (uint)valid.Length)]);
                }
            }
            //appends timestamp to token to use later to add one hour validity to token
            // res.Append(DateTime.Now.ToString("yyyyMMddHHmmss"));
            return res.ToString();
        }
    }
}