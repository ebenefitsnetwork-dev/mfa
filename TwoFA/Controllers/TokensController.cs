﻿using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using Microsoft.AspNetCore.Mvc;
using TwoFA.Data;
using TwoFA.Models;

namespace TwoFA.Controllers
{
    [ApiController]
    public class TokensController : Controller
    {
        private readonly ApplicationDbContext context;

        public TokensController(ApplicationDbContext context)
        {
            this.context = context;
        }

        [HttpGet("api/[controller]/GenerateToken")]
        public IActionResult GenerateToken(string recipientEmail)
        {
            try
            {
                string fromMail = "mfa@ebenefitsnetwork.com";
                string password = "Xv1+1g^A";

                MailMessage mail = new MailMessage(fromMail, recipientEmail);
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;
                NetworkCredential nc = new NetworkCredential(fromMail, password);
                client.Credentials = nc;
                mail.Subject = "Login Pass Code";
                string token = Helpers.RandomString();
                //insert into DB
                Token userToken = new Token
                {
                    Email = recipientEmail,
                    GenerationDateTime = DateTime.Now,
                    TokenCode = token
                };
                foreach (var mytoken in context.Tokens)
                {
                    if (mytoken.Email == recipientEmail)
                    {
                        context.Tokens.Remove(mytoken);
                    }
                }
                context.Tokens.Add(userToken);

                context.SaveChanges();
                mail.Body = "Please use this token to Login: " + token +
                            "\r\n Note: This token is only valid for one hour.";
                client.Send(mail);
                return Ok();
            }
            catch (Exception)
            {
                return Unauthorized();
            }
        }

        [HttpGet("api/[controller]/ValidateToken")]
        public IActionResult ValidateToken(string userEmail, string Token)
        {
            try
            {
                Token temp = context.Tokens.First(token => token.Email == userEmail);
                if (temp != null)
                {
                    if (DateTime.Now.Subtract(temp.GenerationDateTime).TotalHours > 1)
                    {
                        context.Tokens.Remove(temp);
                        return Unauthorized();
                    }
                    if (temp.Email == userEmail && temp.TokenCode == Token)
                    {
                        context.Tokens.Remove(temp);
                        context.SaveChanges();
                        return Ok();
                    }
                }

                return Unauthorized();
            }
            catch (Exception)
            {

                return Unauthorized();
            }
        }
    }
}
