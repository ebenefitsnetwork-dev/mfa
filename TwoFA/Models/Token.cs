﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoFA.Models
{
    public class Token
    {
        public Guid Id { get; set; }
        public string TokenCode { get; set; }
        public DateTime GenerationDateTime { get; set; }
        public string Email { get; set; }
    }
}
